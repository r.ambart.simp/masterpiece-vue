import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import jwt_decode from 'jwt-decode'

Vue.use(Vuex)

const apiUrl = 'http://127.0.0.1:8000';

export default new Vuex.Store({
  state: {
    user: null,
    societies: [],
    reputation: Number,
  },
  mutations: {
    CHANGE_USER(state, user) {
      state.user = user;
    },

    ADD_SOCIETIE(state, societie) {
      state.societies.push(societie);
    },

    FETCH_SOCIETY(state, societie) {
      state.societies = societie;
    },

    // REPUTATION !!!

   ADD_REPUTATION(state, inputSocity){
     let societyResult = state.societies.filter(socity => socity.id == inputSocity.id)
     societyResult.reputation++
   }
      
  },
  actions: {

    // REPUTATION !!!

    async addReputation({commit}, societie) {
      let response = await Axios.patch(apiUrl+'/api/society/'+societie.id, {reputation: societie.reputation+1});
      commit('ADD_REPUTATION', response.data);
  },
  
    //AUTHENTIFICATION !!!

    changeUser({ commit }, user) {
      commit('CHANGE_USER', user);
    },

    /**
     * @param {{username: string, password: string}} credentials 
     */
    async login({ commit }, credentials) {
      let response = await Axios.post(apiUrl+'/api/login_check', credentials);
      localStorage.setItem('token', response.data.token);
      commit('CHANGE_USER', credentials.username);
    },

    async register({commit}, user) {
      let response = await Axios.post(apiUrl+'/register', user);
      commit('CHANGE_USER')
      if(response.status === 201) {
          return true;
      }
      return false;
  },    

    logout({ commit }) {
      localStorage.removeItem('token');
      commit('CHANGE_USER', null);
    },
    checkToken({ commit }) {
      if (localStorage.getItem('token')) {
        let token = jwt_decode(localStorage.getItem('token'));
        if (new Date().getTime() < token.exp * 1000) {
          commit('CHANGE_USER', token.username);
        }else {
          localStorage.removeItem('token');
        }
      }
    },

  

//SOCIETIES !!!

async addSocietie({commit}, societie) {
    let response = await Axios.post(apiUrl+'/api/society', societie);
  commit('ADD_SOCIETIE', response.data);
},
async fetchSociety({commit}) {
  let response = await Axios.get(apiUrl+'/api/society');
  commit('FETCH_SOCIETY', response.data);
},
},


  strict: true,
})
