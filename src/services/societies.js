import Axios from "axios"

export class SocietiesService {

    constructor() {
        this.url = 'http://127.0.0.1:8000/api/society/';
    }

    async findAll() {
        let response = await Axios.get(this.url);
        return response.data;
    }

    async findOne(id){
        let response = await Axios.get(this.url + id);
        return response.data;
    }
 
}