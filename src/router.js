import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Societies from './views/Societies.vue'
import Leaderboard from './views/Leaderboard.vue'
import OneSociety from './views/OneSociety.vue'
import Started from './views/Started.vue'
import OnePerson from './views/OnePerson.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home

    },
    {
      path: '/leaderboard',
      name: 'leaderboard',
      component: Leaderboard
    },
    {
      path: '/societies',
      name: 'societies',
      component: Societies
    },
    {
      path: '/started',
      name: 'started',
      component: Started
    },
    {
      path: '/onesociety/:societyId',
      name: 'onesociety',
      component: OneSociety
    },
    {
      path: '/profil/:id',
      name: 'oneperson',
      component: OnePerson
    },
    
  ],
  scrollBehavior() {
    return {x: 0, y: 0}
}
})
