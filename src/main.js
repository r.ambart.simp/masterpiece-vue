import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import store from './store'
import './auth-interceptor'
import VueMouseParallax from 'vue-mouse-parallax'
import VueMq from 'vue-mq'
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);

Vue.use(VueMq, {
  breakpoints: {
    sm: 450,
    md: 1250,
    lg: Infinity,
  }
})

Vue.use(VueMouseParallax)

Vue.use(Vuetify)

Vue.config.productionTip = false

new Vue({
  router,
  Vuetify,
  store,
  render: h => h(App)
}).$mount('#app')


import moment from 'moment'
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
})